use arb_sdk_rs::blocklistener::Pool;

use bytemuck::try_from_bytes;
use solana_client::rpc_client::RpcClient;
use solana_sdk::pubkey::Pubkey;
use std::collections::hash_map::HashMap;
use std::str::FromStr;
use std::sync::Arc;
use std::sync::RwLock;
use std::thread;

#[path = "./account_struct.rs"]
pub mod account_struct;
use account_struct::LiquidityStateLayoutV4;
// use crank;
use solana_program::program_pack::Pack;
extern crate dotenv;
use spl_token;
pub struct _SubObj {
    pub auth: Pubkey,
    pub market_pubkey: account_decoder::serum::MarketPubkeys,
}
pub fn _get_const_feild_of_pool(
    data: LiquidityStateLayoutV4,
    pool_address: Pubkey,
    store_extra_data: Arc<RwLock<Vec<Vec<String>>>>,
) -> Pool {
    let base_mint = data.base_mint;
    let quote_mint = data.quote_mint;
    let pool_id = format!("solana_raydium_{:?}_{:?}", base_mint, quote_mint);
    let tokens = vec![base_mint.to_string(), quote_mint.to_string()];
    let mut extra_data = vec![];
    extra_data
        .push(String::from_str(dotenv::var("RAY_LQ_POOL_V4_PROGRAM").unwrap().as_str()).unwrap()); //ray_program
    extra_data.push(data.open_orders.to_string());
    extra_data.push(data.target_orders.to_string());
    extra_data.push(data.base_vault.to_string());
    extra_data.push(data.quote_vault.to_string());
    extra_data.push(data.market_program_id.to_string());
    extra_data.push(data.market_id.to_string());
    store_extra_data.write().unwrap().push(extra_data);
    let my_pool = Pool {
        exchange: String::from_str("Raydium").unwrap(),
        pool_id: pool_id,
        pool_address: pool_address.to_string(),
        tokens: tokens,
        reserves: vec![String::from("0"), String::from("0")],
        fee: 0,
        extra_data: String::from(""),
    };
    return my_pool;
}
pub fn _get_extra_data(list_serum_dex: Vec<Pubkey>) -> Vec<_SubObj> {
    let my_rpc = dotenv::var("RPC").unwrap();
    let config = solana_sdk::commitment_config::CommitmentConfig::processed();
    let client = RpcClient::new_with_commitment(my_rpc.as_str(), config);
    let accs_data = client.get_multiple_accounts(&list_serum_dex);
    let mut result = vec![];
    match accs_data {
        Ok(data) => {
            for i in 0..list_serum_dex.len() / 2 {
                let market_state = account_decoder::serum::MarketState::load(
                    &data[2 * i].as_ref().unwrap().data[..],
                );
                let market = list_serum_dex[2 * i];
                let program_id =
                    Pubkey::from_str(dotenv::var("SERUM_DEX_V3").unwrap().as_str()).unwrap();
                let temp = _SubObj {
                    auth: spl_token::state::Mint::unpack(
                        &data[2 * i + 1].as_ref().unwrap().data[..],
                    )
                    .unwrap()
                    .mint_authority
                    .unwrap(),
                    market_pubkey: market_state.get_market_pubkeys(&market, &program_id),
                };
                result.push(temp);
            }
        }
        Err(_) => {}
    }
    return result;
}
pub fn _create_client_rpc() -> RpcClient {
    let my_rpc = dotenv::var("RPC").unwrap();
    let config = solana_sdk::commitment_config::CommitmentConfig::processed();
    let client = RpcClient::new_with_commitment(my_rpc, config);
    return client;
}

pub fn _filter_pool(accounts: Vec<(Pubkey, solana_sdk::account::Account)>) -> Vec<Pubkey> {
    let mut result = vec![];
    let accs_filter = String::from(dotenv::var("TOKEN_MUST_CONTAIN_IN_POOL").unwrap());
    let accs_filter = accs_filter.replace("[", "");
    let accs_filter = accs_filter.replace("]", "");
    let accs_filter: Vec<&str> = accs_filter.split(", ").collect();
    for (address, data) in accounts {
        let decode_acc = try_from_bytes::<LiquidityStateLayoutV4>(&data.data[..]);
        if decode_acc.is_ok() {
            let decode_acc = decode_acc.unwrap();
            for token in &accs_filter {
                if decode_acc.base_mint.to_string() == String::from(token.clone())
                    || decode_acc.quote_mint.to_string() == String::from(token.clone())
                {
                    result.push(address);
                    break;
                }
            }
        }
    }
    return result;
}
pub fn _filter_pool_sdk(
    accounts: Vec<(Pubkey, solana_sdk::account::Account)>,
) -> (HashMap<String, Pool>, HashMap<String, Vec<Pubkey>>) {
    let mut list_pools = HashMap::new();
    let mut store_account_query_reserves = HashMap::new();
    let accs_filter = String::from(dotenv::var("TOKEN_MUST_CONTAIN_IN_POOL").unwrap());
    let accs_filter = accs_filter.replace("[", "");
    let accs_filter = accs_filter.replace("]", "");
    let accs_filter: Vec<&str> = accs_filter.split(", ").collect();
    let my_rpc = dotenv::var("RPC").unwrap();
    let config = solana_sdk::commitment_config::CommitmentConfig::processed();
    let client = RpcClient::new_with_commitment(my_rpc.as_str(), config);

    let count = 0;
    for i in 0..accounts.len() {
        if count == 25 || i == accounts.len() {
            thread::spawn(move || {});
        }
    }

    for (address, data) in accounts {
        let decode_acc = try_from_bytes::<LiquidityStateLayoutV4>(&data.data[..]);
        // let mut store_extra_data = HashMap::new();
        if decode_acc.is_ok() {
            let decode_acc = decode_acc.unwrap();
            for token in &accs_filter {
                if decode_acc.base_mint.to_string() == String::from(token.clone())
                    || decode_acc.quote_mint.to_string() == String::from(token.clone())
                {
                    let mut extra_data = vec![];
                    let fee = ((decode_acc.swap_fee_numerator as f64
                        / decode_acc.swap_fee_denominator as f64)
                        * 1000000.0) as u64;
                    extra_data.push(
                        String::from_str(dotenv::var("RAY_LQ_POOL_V4_PROGRAM").unwrap().as_str())
                            .unwrap(),
                    ); //ray_program
                    extra_data.push(decode_acc.open_orders.to_string());
                    extra_data.push(decode_acc.target_orders.to_string());
                    extra_data.push(decode_acc.base_vault.to_string());
                    extra_data.push(decode_acc.quote_vault.to_string());
                    extra_data.push(decode_acc.market_program_id.to_string());
                    extra_data.push(decode_acc.market_id.to_string());

                    let mut account_query_reserves = vec![];
                    account_query_reserves.push(decode_acc.base_vault);
                    account_query_reserves.push(decode_acc.quote_vault);
                    account_query_reserves.push(decode_acc.open_orders);
                    account_query_reserves.push(address);

                    store_account_query_reserves
                        .insert(address.to_string(), account_query_reserves);
                    let acc_for_fetch_extra_data = [decode_acc.market_id, decode_acc.lp_mint];
                    let sub_data_for_extra_data = client
                        .get_multiple_accounts(&acc_for_fetch_extra_data)
                        .unwrap();
                    let mint_authority = spl_token::state::Mint::unpack(
                        &sub_data_for_extra_data[1].as_ref().unwrap().data[..],
                    )
                    .unwrap()
                    .mint_authority
                    .unwrap();
                    let market_state = account_decoder::serum::MarketState::load(
                        &sub_data_for_extra_data[0].as_ref().unwrap().data[..],
                    );
                    let program_id =
                        Pubkey::from_str(dotenv::var("SERUM_DEX_V3").unwrap().as_str()).unwrap();
                    let market =
                        market_state.get_market_pubkeys(&decode_acc.market_id, &program_id);
                    extra_data.push(market.bids.to_string());
                    extra_data.push(market.asks.to_string());
                    extra_data.push(market.event_q.to_string());
                    extra_data.push(market.coin_vault.to_string());
                    extra_data.push(market.pc_vault.to_string());
                    extra_data.push(market.vault_signer_key.to_string());
                    extra_data.push(mint_authority.to_string());
                    let pool = Pool {
                        exchange: String::from("Raydium"),
                        pool_id: format!(
                            "solana_raydium_{:?}_{:?}",
                            decode_acc.base_mint, decode_acc.quote_mint
                        ),
                        pool_address: address.to_string(),
                        tokens: vec![
                            decode_acc.base_mint.to_string(),
                            decode_acc.quote_mint.to_string(),
                        ],
                        reserves: vec![String::from("0"), String::from("0")],
                        fee: fee,
                        extra_data: serde_json::to_string(&extra_data).unwrap(),
                    };
                    list_pools.insert(pool.pool_address.clone(), pool);
                    break;
                }
            }
        }
    }
    return (list_pools, store_account_query_reserves);
}
