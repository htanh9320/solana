use arrayref::array_refs;
use bytemuck::{bytes_of, from_bytes, try_from_bytes, Pod, Zeroable};
use solana_sdk::pubkey::Pubkey;
use std::mem::size_of;

#[repr(packed)]
#[derive(Copy, Clone, Debug, Default)]
pub struct OpenOrders {
    pub account_flags: u64, // Initialized, OpenOrders
    pub market: [u64; 4],
    pub owner: [u64; 4],

    pub native_coin_free: u64,
    pub native_coin_total: u64,

    pub native_pc_free: u64,
    pub native_pc_total: u64,

    pub free_slot_bits: u128,
    pub is_bid_bits: u128,
    // pub orders: [u128; 128],
    // pub client_order_ids: [u64; 128],
    // pub referrer_rebates_accrued: u64,
}

unsafe impl Pod for OpenOrders {}
unsafe impl Zeroable for OpenOrders {}

impl OpenOrders {
    pub fn load(data: &[u8]) -> Self {
        const SIZE: usize = size_of::<OpenOrders>();
        let (_head, data, _tail) = array_refs!(data, 5, SIZE; ..;);

        try_from_bytes::<Self>(data).unwrap().clone()
    }
}

#[derive(Copy, Clone, Debug)]
#[repr(packed)]
pub struct MarketState {
    // 0
    pub account_flags: u64, // Initialized, Market

    // 1
    pub own_address: [u64; 4],

    // 5
    pub vault_signer_nonce: u64,
    // 6
    pub coin_mint: [u64; 4],
    // 10
    pub pc_mint: [u64; 4],

    // 14
    pub coin_vault: [u64; 4],
    // 18
    pub coin_deposits_total: u64,
    // 19
    pub coin_fees_accrued: u64,

    // 20
    pub pc_vault: [u64; 4],
    // 24
    pub pc_deposits_total: u64,
    // 25
    pub pc_fees_accrued: u64,

    // 26
    pub pc_dust_threshold: u64,

    // 27
    pub req_q: [u64; 4],
    // 31
    pub event_q: [u64; 4],

    // 35
    pub bids: [u64; 4],
    // 39
    pub asks: [u64; 4],

    // 43
    pub coin_lot_size: u64,
    // 44
    pub pc_lot_size: u64,

    // 45
    pub fee_rate_bps: u64,
    // 46
    pub referrer_rebates_accrued: u64,
}

unsafe impl Pod for MarketState {}
unsafe impl Zeroable for MarketState {}

impl MarketState {
    pub fn _load(data: &[u8]) -> Self {
        let (_head, data, _tail) = array_refs!(data, 5, 376; ..;);

        from_bytes::<Self>(data).clone()
    }
}

fn _gen_vault_signer_seeds<'a>(nonce: &'a u64, market: &'a Pubkey) -> [&'a [u8]; 2] {
    [market.as_ref(), bytes_of(nonce)]
}

pub fn _gen_vault_signer_key(
    nonce: u64,
    market: &Pubkey,
    program_id: &Pubkey,
) -> anyhow::Result<Pubkey> {
    let seeds = _gen_vault_signer_seeds(&nonce, market);
    Ok(Pubkey::create_program_address(&seeds, program_id)?)
}

#[derive(Debug)]
pub struct MarketPubkeys {
    pub req_q: Pubkey,
    pub event_q: Pubkey,
    pub bids: Pubkey,
    pub asks: Pubkey,
    pub coin_vault: Pubkey,
    pub pc_vault: Pubkey,
    pub vault_signer_key: Pubkey,
}
#[repr(packed)]
#[derive(Copy, Clone, Debug)]
pub struct MarketStateV2 {
    pub inner: MarketState,
    pub open_orders_authority: Pubkey,
    pub prune_authority: Pubkey,
    pub consume_events_authority: Pubkey,
    // Unused bytes for future upgrades.
    pub _padding: [u8; 992],
}
unsafe impl Pod for MarketStateV2 {}
unsafe impl Zeroable for MarketStateV2 {}
