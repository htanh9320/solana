use arb_sdk_rs::blocklistener::Pool;
use std::collections::hash_map::HashMap;
use serde;
#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct MyPool {
    pub exchange: String,
    pub pool_id: String,
    pub pool_address: String,
    pub tokens: Vec<String>,
    pub reserves: Vec<String>,
    pub fee: f64,
    pub block: u64,
    pub extra_data: Vec<String>,
}
#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct ResFromThread {
    pub block: u64,
    pub pools: HashMap<String, Pool>,
}
#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct NatsResult {
    pub block: u64,
    pub changed_reserves: Vec<PoolChanged>,
}
#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct PoolChanged {
    pub exchange: String,
    pub pool_id: String, // `${chain_id}_${exchange}_[${token}]`
    pub pool_address: String,
    pub tokens: Vec<String>,
    pub reserves: Vec<String>,
    pub fee: f64, // 3000 means 0.3% (old 0.003 means 0.3%)
    pub extra_data: Vec<String>,
}
