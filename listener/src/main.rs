mod crawler;
mod interface;
mod raydium;

use arb_sdk_rs::logger;
use arb_sdk_rs::query::block;
use arb_sdk_rs::query::block::BlockListener;
use arb_sdk_rs::query::exchange::Blockchain;
use arb_sdk_rs::query::exchange::Dex;
use arb_sdk_rs::query::query;
use crawler::Solana;
use futures::executor::block_on;
use solana_client::rpc_client::RpcClient;
use std::thread;

pub struct BlockListenerSol {}
impl BlockListener for BlockListenerSol {
    fn query_block(&self) -> u64 {
        let config = solana_sdk::commitment_config::CommitmentConfig::processed();
        let my_rpc = dotenv::var("RPC").unwrap();
        let client = RpcClient::new_with_commitment(my_rpc, config);
        let block = client.get_block_height();
        if block.is_ok() {
            let block = block.unwrap();
            return block;
        }
        return 0;
    }
}

#[tokio::main]
async fn main() {
    let dex = Dex {
        name: String::from("raydium"),
        address: vec![
            String::from("DjVE6JNiYqPL2QXyCUUh8rNjHrbz9hXHNYt99MQ59qw1"),
            String::from("9W959DqEETiGZocYWCQPaJ6sBmUzgfxXfqGeTEdp3aQP"),
        ],
        fee: 3000,
        batch_size: 25,
    };
    let my_exchange = Solana {};
    let block_listener = BlockListenerSol {};
    thread::spawn(move || {
        let port_grpc = dotenv::var("PORT_GRPC").unwrap();
        query::start_crawler(
            Box::new(block_listener),
            Box::new(my_exchange),
            String::from("listener"),
            String::from("solana"),
            port_grpc.to_string(),
        );
    })
    .join()
    .expect("Thread panicked");
}
