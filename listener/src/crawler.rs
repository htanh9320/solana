use arb_sdk_rs::query::exchange::{Blockchain, Dex, Address, ResFromThread, TokenBalance};
use bigdecimal::BigDecimal;
use bytemuck::try_from_bytes;
use solana_client::rpc_client::RpcClient;
use solana_program::program_pack::Pack;
use solana_sdk::pubkey::Pubkey;
use std::collections::hash_map::HashMap;
use std::str::FromStr;
use std::sync::Arc;
use std::sync::RwLock;
use std::sync::mpsc;
use async_trait::async_trait;
use tokio::sync::broadcast;
use threadpool::ThreadPool;

use crate::raydium::account_struct;
use crate::raydium::state::OpenOrders;
use crate::interface;
use arb_sdk_rs::blocklistener::{Pool, NewReserve};

extern crate dotenv;
#[derive(Debug, Clone)]
pub struct Solana{

}
#[async_trait]
impl Blockchain for Solana{
    fn new(&self){

    }
    fn get_dex(&self)->Vec<Dex>{
        let raydium_dex = Dex{
            name : String::from("raydium"),
            address: vec![String::from("675kPX9MHTjS2zt1qfr1NYHuzeLXfQM9H24wFSUt1Mp8")],
            fee: 2500,
            batch_size: 25
        };
        // let orca_dex = Dex{
        //     name : String::from("orca"),
        //     address: vec![String::from("DjVE6JNiYqPL2QXyCUUh8rNjHrbz9hXHNYt99MQ59qw1"), String::from("9W959DqEETiGZocYWCQPaJ6sBmUzgfxXfqGeTEdp3aQP")],
        //     fee: 3000,
        //     batch_size: 50
        // };
        return vec![raydium_dex];
    }
    
    fn get_pools(&self, dex: Dex) -> HashMap<String, Pool>{
        let mut result = Arc::new(RwLock::new(HashMap::new()));
        let my_rpc = dotenv::var("RPC").unwrap();
        let config = solana_sdk::commitment_config::CommitmentConfig::processed();
        let client = RpcClient::new_with_commitment(my_rpc, config);

        if dex.name == String::from(String::from("raydium")) {
            let sub_key = dotenv::var("RAY_LQ_POOL_V4_PROGRAM").unwrap();
            let ray_lq_pool_v4_program = Pubkey::from_str(sub_key.as_str()).unwrap();
            let pool_data = client.get_program_accounts(&ray_lq_pool_v4_program).unwrap();
            let mut filter_pool_data = vec![];
            for (address, data) in pool_data{
                if data.data.len() == 752{
                    filter_pool_data.push((address, data));
                }
            };
            let mut asigments = vec![];
            let mut sub_batch = vec![];
            let mut count = 0;
            let mut count_loop = 0;
            let pools_num = filter_pool_data.len();
            for (address, account) in filter_pool_data.clone() {
                if count == 50 || count_loop == pools_num - 1 {
                    asigments.push(sub_batch.clone());
                    sub_batch.clear();
                    count = 0;
                } else {
                    sub_batch.push((address, account));
                    count += 1;
                }
                count_loop += 1;
            }
            let thread_pool = ThreadPool::new(100);
            for asigment in asigments{
                let my_rpc = dotenv::var("RPC").unwrap();
                let config = solana_sdk::commitment_config::CommitmentConfig::processed();
                let client = RpcClient::new_with_commitment(my_rpc, config);
                let sub_result = result.clone();
                thread_pool.execute(move||{
                    let mut pool_container = vec![];
                    let mut extra_data_container = vec![];
                    let mut acc_for_fetch_extra_data = vec![];
                    for (address, account) in asigment.clone(){
                        let data = account.clone();
                        let decode_acc = try_from_bytes::<account_struct::LiquidityStateLayoutV4>(
                            &data.data[..],
                        );
                        if decode_acc.is_ok() {
                            let decode_acc = decode_acc.unwrap();
                            if decode_acc.lp_mint != Pubkey::from_str("11111111111111111111111111111111").unwrap(){
                                let mut extra_data = vec![];
                                let fee = ((decode_acc.swap_fee_numerator as f64
                                    / decode_acc.swap_fee_denominator as f64)
                                    * 1000000.0) as u64;
                                extra_data.push(
                                    String::from_str(dotenv::var("RAY_LQ_POOL_V4_PROGRAM").unwrap().as_str())
                                        .unwrap(),
                                ); //ray_program
                                extra_data.push(decode_acc.open_orders.to_string());
                                extra_data.push(decode_acc.target_orders.to_string());
                                extra_data.push(decode_acc.base_vault.to_string());
                                extra_data.push(decode_acc.quote_vault.to_string());
                                extra_data.push(decode_acc.market_program_id.to_string());
                                extra_data.push(decode_acc.market_id.to_string());
                                extra_data_container.push(extra_data);
                                acc_for_fetch_extra_data.push(decode_acc.market_id);
                                acc_for_fetch_extra_data.push(decode_acc.lp_mint);

                                let pool = Pool {
                                    exchange: String::from("Raydium"),
                                    pool_id: format!(
                                        "solana_raydium_{:?}_{:?}",
                                        decode_acc.base_mint, decode_acc.quote_mint
                                    ),
                                    pool_address: address.clone().to_string(),
                                    tokens: vec![
                                        decode_acc.base_mint.to_string(),
                                        decode_acc.quote_mint.to_string(),
                                    ],
                                    reserves: vec![String::from("0"), String::from("0")],
                                    fee: fee,
                                    extra_data: String::from(""),
                                };
                                pool_container.push(pool);
                            }
                        }
                    }
                    let sub_data_for_extra_data = client
                            .get_multiple_accounts(&acc_for_fetch_extra_data)
                            .unwrap();
                    for i in 0..acc_for_fetch_extra_data.len() / 2 {
                        let market_state = account_decoder::serum::MarketState::load(
                            &sub_data_for_extra_data[2 * i].as_ref().unwrap().data[..],
                        );
                        let program_id =
                            Pubkey::from_str(dotenv::var("SERUM_DEX_V3").unwrap().as_str()).unwrap();
                        let market_pubkey = market_state.get_market_pubkeys(&acc_for_fetch_extra_data[2*i], &program_id);
                        extra_data_container[i].push(market_pubkey.bids.to_string());
                        extra_data_container[i].push(market_pubkey.asks.to_string());
                        extra_data_container[i].push(market_pubkey.event_q.to_string());
                        extra_data_container[i].push(market_pubkey.coin_vault.to_string());
                        extra_data_container[i].push(market_pubkey.pc_vault.to_string());
                        extra_data_container[i].push(market_pubkey.vault_signer_key.to_string());
                        extra_data_container[i].push(spl_token::state::Mint::unpack(&sub_data_for_extra_data[2 * i + 1].as_ref().unwrap().data[..]).unwrap().mint_authority.unwrap().to_string());
                        pool_container[i].extra_data = serde_json::to_string(&extra_data_container[i].clone()).unwrap();
                    }
                    for pool in pool_container{
                        sub_result.write().unwrap().insert(pool.pool_address.clone(), pool.clone());
                    }
                });
            }
            thread_pool.join();      
        } else if dex.name == String::from(String::from("orca")) {
            let orca_token_swap_1 =
                Pubkey::from_str(dotenv::var("ORCA_TOKEN_SWAP_1").unwrap().as_str()).unwrap();
            let orca_token_swap_2 =
                Pubkey::from_str(dotenv::var("ORCA_TOKEN_SWAP_2").unwrap().as_str()).unwrap();
            let accounts_v1 = client.get_program_accounts(&orca_token_swap_1);
            let accounts_v2 = client.get_program_accounts(&orca_token_swap_2);
            let mut accounts: Vec<(Pubkey, solana_sdk::account::Account)> = vec![];
            if accounts_v1.is_ok() && accounts_v2.is_ok() {
                accounts = accounts_v1.unwrap();
                let accounts_v2 = accounts_v2.unwrap();
                for item in accounts_v2 {
                    accounts.push(item);
                }
            }

            let pool_data = accounts;

            for i in 0..pool_data.len() {
                let decode_acc = spl_token_swap::state::SwapV1::unpack_from_slice(
                    &pool_data[i].1.clone().data[1..],
                )
                .unwrap();
                let mut store_acc_for_fetch_reserves = vec![];
                store_acc_for_fetch_reserves.push(decode_acc.token_a);
                store_acc_for_fetch_reserves.push(decode_acc.token_b);
                let trade_fee = ((decode_acc.fees.trade_fee_numerator as f64)
                    / (decode_acc.fees.trade_fee_denominator as f64)
                    * 1000000.0) as u64;
                let owner_trade_fee = ((decode_acc.fees.owner_trade_fee_numerator as f64)
                    / (decode_acc.fees.owner_trade_fee_denominator as f64)
                    * 1000000.0) as u64;
                let fee = trade_fee + owner_trade_fee;
                let mut extra_data = vec![];
                extra_data.push(decode_acc.pool_mint.to_string());
                extra_data.push(decode_acc.pool_mint.to_string());
                extra_data.push(decode_acc.token_a.to_string());
                extra_data.push(decode_acc.token_b.to_string());
                extra_data.push(decode_acc.pool_fee_account.to_string());

                let pool = Pool {
                    exchange: String::from("Orca"),
                    pool_id: format!(
                        "solana_raydium_{:?}_{:?}",
                        decode_acc.token_a_mint, decode_acc.token_b_mint
                    ),
                    pool_address: pool_data[i].0.to_string(),
                    tokens: vec![
                        decode_acc.token_a_mint.to_string(),
                        decode_acc.token_b_mint.to_string(),
                    ],
                    reserves: vec![String::from("0"), String::from("0")],
                    fee: fee,
                    extra_data: serde_json::to_string(&extra_data).unwrap(),
                };
                result.write().unwrap().insert(pool_data[i].0.to_string(), pool);
            }
        }
        return result.read().unwrap().clone();
    }

    fn get_reserves(
        &self,
        pool_address: Vec<String>,
        related_address: Vec<String>,
        dex: Dex,
    ) -> HashMap<String, (BigDecimal, BigDecimal)>{
        let mut result: HashMap<String, (BigDecimal, BigDecimal)> = HashMap::new();
        let my_rpc = dotenv::var("RPC").unwrap();
        let config = solana_sdk::commitment_config::CommitmentConfig::processed();
        let client = RpcClient::new_with_commitment(my_rpc, config);
        let mut sub_related_address = vec![];
        for address in related_address {
            sub_related_address.push(Pubkey::from_str(address.as_str()).unwrap());
        }
        if dex.name == String::from("raydium") {
            let data_reserves = client.get_multiple_accounts(sub_related_address.as_slice());
            let mut count = 0;
            if data_reserves.is_ok() {
                let data_reserves = data_reserves.unwrap();
                let mut poiter = data_reserves.iter();
                loop {
                    let mut token_a_reserves = 0;
                    let mut token_b_reserves = 0;
                    let token_balance_0 = spl_token::state::Account::unpack_from_slice(
                        &poiter.next().unwrap().as_ref().unwrap().data[..],
                    )
                    .unwrap();
                    let token_balance_1 = spl_token::state::Account::unpack_from_slice(
                        &poiter.next().unwrap().as_ref().unwrap().data[..],
                    )
                    .unwrap();
                    token_a_reserves += token_balance_0.amount;
                    token_b_reserves += token_balance_1.amount;
                    let result_reserves = OpenOrders::load(
                        &poiter.next().unwrap().as_ref().unwrap().data[..],
                    );
                    token_a_reserves += result_reserves.native_coin_total;
                    token_b_reserves += result_reserves.native_pc_total;
                    let decoded_data =
                        try_from_bytes::<account_struct::LiquidityStateLayoutV4>(
                            &poiter.next().unwrap().as_ref().unwrap().data[..],
                        )
                        .unwrap();
                    token_a_reserves -= decoded_data.base_need_take_pnl;
                    token_b_reserves -= decoded_data.quote_need_take_pnl;
                    result.insert(
                        pool_address[count / 4].clone(),
                        (
                            bigdecimal::FromPrimitive::from_u64(token_a_reserves).unwrap(),
                            bigdecimal::FromPrimitive::from_u64(token_b_reserves).unwrap(),
                        ),
                    );
                    count += 4;
                    if count == data_reserves.len() {
                        break;
                    }
                }
            }
        } else if dex.name == String::from("orca") {
            let data_reserves = client.get_multiple_accounts(sub_related_address.as_slice());
            let mut count = 0;
            if data_reserves.is_ok() {
                let data_reserves = data_reserves.unwrap();
                let mut poiter = data_reserves.iter();
                loop {
                    let mut token_a_reserves = 0;
                    let mut token_b_reserves = 0;
                    let token_balance_0 = spl_token::state::Account::unpack_from_slice(
                        &poiter.next().unwrap().as_ref().unwrap().data[..],
                    )
                    .unwrap();
                    let token_balance_1 = spl_token::state::Account::unpack_from_slice(
                        &poiter.next().unwrap().as_ref().unwrap().data[..],
                    )
                    .unwrap();
                    token_a_reserves += token_balance_0.amount;
                    token_b_reserves += token_balance_1.amount;
                    result.insert(
                        pool_address[count / 2].clone(),
                        (
                            bigdecimal::FromPrimitive::from_u64(token_a_reserves).unwrap(),
                            bigdecimal::FromPrimitive::from_u64(token_b_reserves).unwrap(),
                        ),
                    );
                    count += 2;
                    if count == data_reserves.len() {
                        break;
                    }
                }
            }
        }
        return result;
    }
    async fn crate_thread(&self, pools: HashMap<String, Pool>, mut listen_block: broadcast::Receiver<u64>, sender_to_server: mpsc::Sender<ResFromThread>, sender_nats: mpsc::Sender<NewReserve>, dex: Dex){
        let mut pools_address = vec![];
        if dex.name == "raydium"{
            let state_thread = Arc::new(RwLock::new(pools));
            let mut related_acocunt: Vec<String> = vec![];

            for (address, pool) in state_thread.read().unwrap().iter(){
                pools_address.push(address.clone());
                let extra_data = pool.extra_data.clone();
                let decode_extra_data: Vec<String> = serde_json::from_str(extra_data.as_str()).unwrap();
                related_acocunt.push(decode_extra_data[3].clone());
                related_acocunt.push(decode_extra_data[4].clone());
                related_acocunt.push(decode_extra_data[1].clone());
                related_acocunt.push(address.clone());
            }
            loop{
                let new_block = listen_block.recv().await;
                if new_block.is_ok(){
                    let new_block = new_block.unwrap();
                    let mut res_nats = NewReserve {
                        block: new_block.clone(),
                        changed_reserves: vec![],
                    }; 
                    let new_reserves = self.get_reserves(pools_address.clone(), related_acocunt.clone(), dex.clone());
                    for (address,reserves) in new_reserves{
                        let sub_reserves = vec![reserves.0.to_string(), reserves.1.to_string()];
                        if state_thread.read().unwrap().get(&address).unwrap().reserves != sub_reserves{
                            state_thread.write().unwrap().get_mut(&address).unwrap().reserves = sub_reserves;
                            res_nats.changed_reserves.push(state_thread.read().unwrap().get(&address).unwrap().clone());
                        }
                    }
                    let res_to_grpc_processor = ResFromThread {
                        block: new_block.clone(),
                        pools: state_thread.read().unwrap().clone()
                    };
                    let _nats = sender_nats.send(res_nats);
                    let _grpc = sender_to_server.send(res_to_grpc_processor);
                }
            }
        }
    }
    fn get_tokens_balance(&self, tokens: Vec<String>, owner: String) -> Vec<TokenBalance>{
        let result = vec![];
        return result;
    }
}