use arb_sdk_rs::blocklistener::{NewTrade, NewTradePool, TradeStep};
use arb_sdk_rs::txbroadcast::tx_broadcast::*;
use async_trait::async_trait;

#[derive(Clone)]
pub struct SolBroadcaster {}
#[async_trait]
impl Broadcaster for SolBroadcaster {
    async fn send_tx(&self, trade_steps: Vec<TradeStep>) -> (TxHash, TxErr) {
        let result = (String::from("a"), String::from("a"));
        return result;
    }

    async fn tx_status(&self, trade_steps: Vec<TradeStep>, tx_hash: TxHash) -> NewTrade {
        let trade_pool = NewTradePool {
            address: String::from("asdas"),
            dex: String::from("orca"),
            tokens: vec![String::from("kaka"), String::from("kaka")],
        };
        let result = NewTrade {
            chain: String::from("solana"),
            block_number: 123123,
            block_time: 123123,
            tx_hash: String::from("a"),
            dex: vec![String::from("raydium"), String::from("orca")],
            amount_in: String::from("0"),
            amount_out: String::from("0"),
            err_msg: String::from("err"),
            pools: vec![trade_pool],
            profit_token: String::from("a"),
            profit_token_decimal: 16,
            server_id: String::from("asdas"),
            status: 1,
        };
        return result;
    }

    fn namespace(&self) -> String {
        return String::from("oke");
    }

    fn chain(&self) -> String {
        return String::from("solana");
    }

    fn average_block_time(&self) -> u64 {
        return 0;
    }
}
